Computational study for the article [An Alternating Method for Cardinality-Constrained
Optimization: A Computational Study for the Best
Subset Selection and Sparse Portfolio Problems](http://www.optimization-online.org/DB_HTML/2020/11/8124.html).
In particular, two computational studies are conducted: for the sparse portfolio optimization problem and 
for the best subset selection problem.

# Sparse Portfolio Optimization
### Dependencies

##### Python packages
* numpy
* gurobi 9.0.3

### Test sets

To test the PADM on mean-variance (MV) portfolio optimization problems, 
we use the following three testsets:

1. Five MV portfolio optimization instances described in 
T. Chang, N. Meade, J. Beasley, and Y. Sharaiha. [“Heuristics for cardinality
constrained portfolio optimisation.”](
https://www.sciencedirect.com/science/article/pii/S030505489900074X) 
In: Computers & Operations Research, 27.13 (2000), pp. 1271–1302.
And publicly available in the OR-Library:
J. E. Beasley. [“OR-Library: Distributing Test Problems by Electronic 
Mail.”](https://www.jstor.org/stable/2582903?origin=crossref&seq=1#metadata_info_tab_contents)
In: The Journal of the Operational Research Society 41.11 (1990), pp. 1069–1072.

2. 90 MV portfolio optimization instances described in
A. Frangioni and C. Gentile. [“SDP diagonalizations and perspective cuts for
a class of nonseparable MIQP.”](
https://www.sciencedirect.com/science/article/abs/pii/S0167637706000502?via%3Dihub) 
In: Operations Research Letters 35.2 (2007), pp. 181–185.
The data is available at <http://groups.di.unipi.it/optimize/Data/MV.html>.

3. 60 large-scale randomly generated MV portfolio instances. For that, we follow 
the procedure given in M. Hirschberger, Y. Qi, and R. E. Steuer. 
[“Randomly generating portfolio-selection covariance matrices with 
specified distributional characteristics.”](
https://www.sciencedirect.com/science/article/abs/pii/S0377221705006600?via%3Dihub) 
In:European Journal of Operational Research 177.3 (2007), pp. 1610–1625.

### Reproducing experiments from the paper

We assume that `dir_name` = /your_path/sparse_portfolio_optimization

The results will be of two types: either .csv tables with all the results over 
all instances of the experiment and/or .tex tables with the average 
results.

Reproducing experiments with  **testset 1**:
1. the data sets *port1*, *port2*, *port3*, *port4*, and *port5* need to be 
located in dir_name/data/port_or/ within each 
individual folder. The data is then encoded into two files *mu* and *Q*,
the mean returns and covariance matrix, respectively. We 
include the data set *port1* as an example. All the others must be 
uploaded from <http://people.brunel.ac.uk/~mastjjb/jeb/orlib/portinfo.html>.
2. having the data uploaded, the user can 
set her/his own parameters at the top of the
python script *port_data_experiment.py* located in 
dir_name/code/. The most important 
parameter is `dir_name` which is the user directory where this 
python package has been saved. With the parameters set, 
the user can run `python3 port_data_experiment.py` inside dir_name/code/. 
The results will be in dir_name/results/port-data/.


Reproducing experiments with **testset 2**:
1. the MV instances must be located in dir_name/data/Frangioni_Gentile/MV/ 
within each individual folder according to the number of assets.
As an example, we include the data set *pard200_a*. All the 
other data sets must be uploaded from 
<http://groups.di.unipi.it/optimize/Data/MV.html> following the
README.txt files there.
The diagonal matrices that we use are the ones in the folder "s" and they 
should be located in 
dir_name/data/Frangioni_Gentile/diagonals/s/. 
2. Having the data, the user can set the parameters for the 
experiment (plain PADM) at the top of the 
python script *fr_ge_pure_padm_experiment.py* located in 
dir_name/code/. And now the experiment can be run 
`python3 fr_ge_pure_padm_experiment.py`. 
3. To run the Perspective-PADM experiment, first set the parameters 
at the top of the python script 
*fr_ge_padm_with_perspec.py* and then run 
`python3 fr_ge_padm_with_perspec.py`.


Reproducing experiments with **testset 3**:
1. the random portfolio instances need to be generated following the 
README file contained in 
dir_name/data/large-random/randomportfolio-master. 
The implementation of this procedure was made available by 
Sertalp B. Cay at <http://sertalpbilal.github.io/randomportfolio/>.
60 instances must be generated, being 10 for each number of assets `n` 
in {400, 600, 1000, 2000, 3000, 5000}, each one with a different seed `s`. 
As an example, we include the first random instance: `n`= 400, `s`=1 
in the folder dir_name/data/large-random/400/400-1/.
The seeds that we used: 

- for n = 400, s in {1, 2, ..., 10}
- for n = 600, s in {11, 12, ..., 20}
- for n = 1000, s in {21, 22, ..., 30}
- for n = 2000, s in {41, 42, ..., 50}
- for n = 3000, s in {51, 52, ..., 60}
- for n = 5000, s in {31, 32, ..., 40}
 
2. Now, the parameters need to be adjusted at the top of the python 
script *large-random-experiment.py* in dir_name/code/. 
Then the large-random experiment can be run for each `n`, i. e., 
`python3 large-random-experiment.py`. The results will be 
in dir_name/results/results-random-data/.

3. And to run the experiment where we give Gurobi the time limit 
as the time required by PADM, then set the parameters at the top of the script 
*obj-compare-large-data.py* and run 
`python3 obj-compare-large-data.py` inside dir_name/code/. 

# Best subset selection
### Dependencies
##### Python dependencies

* [pianopy](https://gitlab.com/Dnis/pianopy) == 0.8.4.6.2
* [digitallab](https://gitlab.com/Dnis/dlab) == 1.4.3.3
* numpy
* rpy2

The package [pianopy](https://gitlab.com/Dnis/pianopy) is a Python wrapper 
around the package [piano](https://gitlab.com/Dnis/piano), which is the main package where the PADM is implemented. 
You can get the correct version of pianopy by cloning the git and then calling
`git checkout computational-study-padm-for-cardinality-constrained-problems` or
`git checkout 0.8.4.6.2`. The Python package digitallab is available at
PyPI and can be installed via `pip install --user digitallab==1.4.3.3`.
    
##### R dependencies
    
* [L0Learn](https://github.com/hazimehh/L0Learn)

##### Other dependecies

* Mongo database  
* CPLEX for pianopy 

### Test sets
All test sets are synthetic and are generated while running the code.

### Reproducing experiments from the paper
##### MongoDB config
First create a file `mongodb_credentials.json` in the directory`best_subset_selection`. Then copy-paste

    {
        "user_name": "<YOUR MONGODB USER NAME>",
        "password": "<YOUR MONGODB PASSOWRD>",
        "database": "<DATABASE WHERE RESULTS ARE STORED>",
        "server": "<SERVER ADDRESS OF MONGODB>",
        "auth_mechanism": "SCRAM-SHA-1"
    }

into the file and swap the term `<...>` with your credentials for your MongoDB.

##### Running experiments
Navigate into the directory `best_subset_selection/running_experiments` and execute `run.py`.

##### Evaluating Experiments
Navigate into the directory `best_susbet_selection/evaluation` and call
* `performance_profile_plots.py`
* `relative_sparsity.py`
* `running_time_table.py`
* `selection_accuracy_plots.py`

The four Python files will produce the plots and the table. 



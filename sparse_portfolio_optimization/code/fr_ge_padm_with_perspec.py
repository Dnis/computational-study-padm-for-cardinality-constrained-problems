#!/usr/bin/env python3

""" Perspective-PADM experiment on the A. Frangioni, C. Gentile portfolio instances.

This script runs the experiment in which we compute partial minima
with the variant Perspective-PADM.
The output will be a .csv table with the results.

The user can set the parameters right below.
"""

import numpy as np
from fr_ge.get_fr_ge_data import get_data
from fr_ge.padm_perspective_misocp import padm_perspective
from fr_ge.csv_tab_fr_ge_padm_persp import write_csv_table


def set_parameters():
    """ set parameters for experiment"""
    dir_name = "/home/userName/userRepos/sparse-portfolio"  # the user directory
    nr_assets = 200  # n in {200,300,400}
    card_k = 5  # k in {5,10,15}
    init_penalty = 1  # initial penalty parameter
    time_limit = 600  # time limit in seconds

    return dir_name, nr_assets, card_k, init_penalty, time_limit


def write_iterations(dir_name, data_set, list_adm_iter, k):
    np.savetxt(dir_name + "/results/results-FrGe-data/PADM-PERSP/iters/" + str(data_set) + "_" + str(k),
               list_adm_iter)


def apply_methods(dir_name, nr_assets, k, init_penalty, time_limit):
    dict_run_time = {}
    dict_obj_value = {}
    dict_outer_iters = {}
    dict_adm_iters = {}

    if nr_assets == 200:
        data_names = ["200$^+$", "200$^0$", "200$^-$"]
        data_sets = [["pard200_a", "pard200_b", "pard200_c", "pard200_d", "pard200_e",
                       "pard200_f", "pard200_g", "pard200_h", "pard200_i", "pard200_j"],
                      ["orl200-005-a", "orl200-005-b", "orl200-005-c", "orl200-005-d", "orl200-005-e",
                       "orl200-005-f", "orl200-005-g", "orl200-005-h", "orl200-005-i", "orl200-005-j"],
                      ["orl200-05-a", "orl200-05-b", "orl200-05-c", "orl200-05-d", "orl200-05-e",
                      "orl200-05-f", "orl200-05-g", "orl200-05-h", "orl200-05-i", "orl200-05-j"]]

    elif nr_assets == 300:
        data_names = ["300$^+$", "300$^0$", "300$^-$"]
        data_sets = [["pard300_a", "pard300_b", "pard300_c", "pard300_d", "pard300_e",
                     "pard300_f", "pard300_g", "pard300_h", "pard300_i", "pard300_j"],
                     ["orl300_005_a", "orl300_005_b", "orl300_005_c", "orl300_005_d", "orl300_005_e",
                     "orl300_005_f", "orl300_005_g", "orl300_005_h", "orl300_005_i", "orl300_005_j"],
                     ["orl300_05_a", "orl300_05_b", "orl300_05_c", "orl300_05_d", "orl300_05_e",
                     "orl300_05_f", "orl300_05_g", "orl300_05_h", "orl300_05_i", "orl300_05_j"]]

    elif nr_assets == 400:
        data_names = ["400$^+$", "400$^0$", "400$^-$"]
        data_sets = [["pard400_a", "pard400_b", "pard400_c", "pard400_d", "pard400_e",
                     "pard400_f", "pard400_g", "pard400_h", "pard400_i", "pard400_j"],
                     ["orl400_005_a", "orl400_005_b", "orl400_005_c", "orl400_005_d", "orl400_005_e",
                     "orl400_005_f", "orl400_005_g", "orl400_005_h", "orl400_005_i", "orl400_005_j"],
                     ["orl400_05_a", "orl400_05_b", "orl400_05_c", "orl400_05_d", "orl400_05_e",
                     "orl400_05_f", "orl400_05_g", "orl400_05_h", "orl400_05_i", "orl400_05_j"]]

    methods = [padm_perspective]

    data_counter = 0
    for data_name in data_names:
        for data_set in data_sets[data_counter]:

            vec_return, matrix_sigma, required_ret, diag_vector = get_data(dir_name, data_set, nr_assets)

            for method in methods:
                if method.__name__ == "padm_perspective":
                    [list_adm_iter, counter_outer_iter, time, obj_val] = padm_perspective(nr_assets, vec_return,
                                                                                          required_ret, matrix_sigma,
                                                                                          diag_vector, k,
                                                                                          init_penalty, time_limit)
                    write_iterations(dir_name, data_set, list_adm_iter, k)
                    dict_run_time[data_set, k, "padm_perspective"] = time
                    dict_obj_value[data_set, k, "padm_perspective"] = obj_val
                    dict_outer_iters[data_set, k, "padm_perspective"] = counter_outer_iter
                    dict_adm_iters[data_set, k, "padm_perspective"] = list_adm_iter

        data_counter += 1

    write_csv_table(dir_name, data_names, data_sets, nr_assets, k, methods, dict_run_time, dict_obj_value,
                    init_penalty, dict_outer_iters)


def main():
    dir_name, nr_assets, card_k, init_penalty, time_limit = set_parameters()
    apply_methods(dir_name, nr_assets, card_k, init_penalty, time_limit)


if __name__ == '__main__':
    main()

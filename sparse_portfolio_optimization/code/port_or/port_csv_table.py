#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import csv
import os


def build_csv_table(data_sets, nr_assets, list_k, run_time, obj_value, penalty, outeriters, name_dir):
    os.chdir(name_dir + "/results/port-data/")

    with open("port-or" + ".csv", "w") as csvfile:
        filewriter = csv.writer(csvfile, delimiter='|',
                                quotechar='|', quoting=csv.QUOTE_MINIMAL)
        filewriter.writerow(['instances', 'nr_assets', 'k', 'PADM time(s)', 'bigm time(s)',
                             'PADM objval', 'bigm objval', 'out_iters', 'penalty_initial'])
        for data_set in data_sets:
            for k in list_k:
                filewriter.writerow([data_set, str(nr_assets[data_set]), str(k), str(run_time[
                                    data_set, k, 'padm']), str(run_time[data_set, k, 'bigm_solver']), str(
                                    obj_value[data_set, k, 'padm']), str(obj_value[data_set, k, 'bigm_solver']),
                                    str(outeriters[data_set, k]), str(penalty)])
            filewriter.writerow([])

#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os


def build_table(data_sets, methods, list_k, list_n, run_time, obj_value, name_dir):
   
    os.chdir(name_dir + "/results/port-data/")
    content = r"""\documentclass{article}
    \usepackage{siunitx}
    \usepackage{booktabs}

    \begin{document}"""

    content += r"""
    \begin{tabular}{cccccccc} \toprule 
    Instance & $n$ & $k$ & \multicolumn{2}{c}{PADM} & & \multicolumn{2}{c}{
    Big-M} \\ \cmidrule{4-5} \cmidrule{7-8} & & & Time & Obj Value & & Time & Obj Value
    \\ """

    for data_set in data_sets:

        n = list_n[data_set]

        content += r""" \midrule \textsf{""" + str(data_set) + '} & $' + str(n) + '$ & '

        for k in list_k:

            content += str(k)
            for method in methods:
                if method.__name__ == 'padm':
                    content += r""" & \sisetup{round-mode=places,round-precision=2} \num{""" + str(
                        run_time[data_set, k, "padm"]) + r"""} & \sisetup{round-mode=places,round-precision=5} \num{
                        """+ str(
                            obj_value[data_set, k, "padm"]) + '} &'
                else:
                    content += r""" & \sisetup{round-mode=places,round-precision=2} \num{""" + str(
                            run_time[data_set, k, "bigm_solver"]) + r"""} & \sisetup{round-mode=places,
                            round-precision=5} \num{""" + str(
                         obj_value[data_set, k, "bigm_solver"]) + '}'
            if k == list_k[-1]:
                content += r""" \\ """
            else:
                content += r""" \\ & & """

    content += r"""\bottomrule \end{tabular}"""
    content += r""" \end{document}"""

    with open('port-or.tex', 'w') as file:
        file.write(content)


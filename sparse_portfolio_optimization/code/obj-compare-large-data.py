#!/usr/bin/env python3

"""" This script runs the experiment in which we compare the
quality of the partial minimum computed with PADM
to the best solution that Gurobi could find within
the time that PADM required to compute the partial minimum.
The output results will be two tables:
"ratio-n.csv" and "ratio-n.tex".

The user can set her/his own parameters right below.
"""

from large_random.setting_data import get_data
from large_random.padm import padm
from large_random.bigm_solver import bigm_solver
from large_random.csv_tab_ratio_obj import build_csv_table


def set_parameters():
    """ set parameters for experiment"""
    dir_name = "/home/userName/userRepos/sparse-portfolio"  # the user directory
    nr_assets = 400  # for the paper we test n in {400,600,1000,2000,3000,5000}
    list_k = [5, 10, 50, 100, 200]  # for the paper k in {5,10,50,100,200}
    initial_penalty = 1  # value of initial penalty parameter

    return dir_name, nr_assets, list_k, initial_penalty


def run_experiment(dir_name, nr_assets, list_k, initial_penalty):
    dict_data = {400: 1, 600: 11, 1000: 21, 2000: 41, 3000: 51, 5000: 31}
    nr_random = 10  # number of random instances generated for each n, for the paper:10
    data_sets = [str(nr_assets) + "-" + str(i + dict_data[nr_assets]) for i in range(nr_random)]

    methods = [padm, bigm_solver]

    run_time = {}
    obj_value = {}
    outer_iters = {}
    dict_adm_iters = {}
    gap = {}

    for data_set in data_sets:

        nr_assets, returns, matrix_sigma, r_min = get_data(dir_name + "/data/large_random/" + str(nr_assets) + "/" +
                                                           data_set + "/")
        for k in list_k:
            for method in methods:
                if method.__name__ == 'bigm_solver':

                    [obj_val_solver, gap_solver, time_solver] = bigm_solver(nr_assets, returns, r_min,
                                                                            matrix_sigma, k, padm_time)
                    run_time[data_set, k, 'miqp_solver'] = time_solver
                    obj_value[data_set, k, 'miqp_solver'] = obj_val_solver
                    gap[data_set, k] = gap_solver

                if method.__name__ == 'padm':

                    [list_adm_iter, outer_iter, time, obj_val_padm] = padm(nr_assets, returns, r_min, matrix_sigma, k,
                                                                           initial_penalty, time_limit=3600)

                    #write_iterations(data_set, k, list_adm_iter, outer_iter, rho_initial)
                    run_time[data_set, k, 'padm'] = time
                    padm_time = time
                    obj_value[data_set, k, 'padm'] = obj_val_padm
                    outer_iters[data_set, k] = outer_iter
                    dict_adm_iters[data_set, k] = list_adm_iter

    build_csv_table(dir_name, nr_random, data_sets, nr_assets, list_k, run_time, obj_value, initial_penalty,
                    outer_iters, gap)


def main():
    dir_name, nr_assets, list_k, initial_penalty = set_parameters()
    run_experiment(dir_name, nr_assets, list_k, initial_penalty)


if __name__ == '__main__':
    main()

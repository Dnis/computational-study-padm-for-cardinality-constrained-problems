#!/usr/bin/env python3

""" PADM experiment on the five MV portfolio instances (port-data).

This script runs the experiment in which we compare PADM
to the big-M MIQP formulation solved with Gurobi.
The output results will be two tables: "port-or.csv" and "port-or.tex".

The user can set her/his own parameters right below.
"""

import numpy as np
from port_or.setting_data import get_data
from port_or.padm import padm
from port_or.bigm_solver import bigm_solver
from port_or.port_csv_table import build_csv_table
from port_or.tex_table_port_or import build_table


def set_parameters():
    dir_name = "/home/userName/userRepos/sparse-portfolio"  # the user directory
    penalty_init = 1e-4  # value of initial penalty parameter
    time_limit = 3600  # in seconds

    # there is no need to change the following parameters
    data_sets = ["port1", "port2", "port3", "port4", "port5"]
    list_k = [5, 10, 20]
    methods = [padm, bigm_solver]

    return data_sets, list_k, penalty_init, time_limit, methods, dir_name


def write_iterations(data_set, k, list_adm_iter, dir_name):
    """write to a .txt file the inner ADM iterations in each outer loop"""
    np.savetxt(dir_name + "/results/port-data/iters/test" + str(data_set) + '_' + str(k), list_adm_iter)


def apply_methods(data_sets, list_k, penalty_init, time_limit, methods, dir_name):
    # dicts to store results
    dict_nr_assets = {}
    runtime = {}
    objvalue = {}
    outeriters = {}
    admiters = {}

    for data_set in data_sets:
        nr_assets, vec_returns, matrix_sigma, expected_ret = get_data(dir_name + "/data/port_or/" + data_set + "/")
        dict_nr_assets[data_set] = nr_assets
        for k in list_k:
            for method in methods:
                if method.__name__ == "padm":
                    list_adm_iter, counter_outer_iter, time, obj_val_padm = padm(nr_assets, vec_returns, expected_ret,
                                                                                 matrix_sigma, k, penalty_init,
                                                                                 time_limit)
                    #write_iterations(data_set, k, list_adm_iter, dir_name)
                    runtime[data_set, k, "padm"] = time
                    objvalue[data_set, k, "padm"] = obj_val_padm
                    outeriters[data_set, k] = counter_outer_iter
                    admiters[data_set, k] = list_adm_iter

                if method.__name__ == "bigm_solver":
                    obj_val_bigm, time_bigm = bigm_solver(nr_assets, vec_returns, expected_ret, matrix_sigma, k,
                                                          time_limit)
                    runtime[data_set, k, "bigm_solver"] = time_bigm
                    objvalue[data_set, k, "bigm_solver"] = obj_val_bigm

    build_csv_table(data_sets, dict_nr_assets, list_k, runtime, objvalue, penalty_init, outeriters, dir_name)
    build_table(data_sets, methods, list_k, dict_nr_assets, runtime, objvalue, dir_name)


def main():
    data_sets, list_k, penalty_init, time_limit, methods, dir_name = set_parameters()
    apply_methods(data_sets, list_k, penalty_init, time_limit, methods, dir_name)


if __name__ == '__main__':
    main()

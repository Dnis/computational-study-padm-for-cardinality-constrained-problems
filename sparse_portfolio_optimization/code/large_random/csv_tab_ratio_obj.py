#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import csv
import os


def build_csv_table(dir_name, nr_random, data_sets, nr_assets, list_k, run_time, obj_value, rho_initial,
                    outer_iters, gap):

    os.chdir(dir_name + "/results/results-random-data/")

    with open('ratio_obj-' + str(nr_assets) + '.csv', 'w') as csvfile:
        filewriter = csv.writer(csvfile, delimiter='|',
                                quotechar='|', quoting=csv.QUOTE_MINIMAL)
        filewriter.writerow(['instances', 'nr_assets', 'k', 'PADM time(s)', 'gurobi time(s)',
                             'PADM objval', 'gurobi objval', 'gurobi gap(perc.)', 'out_iters', 'rho_initial'])
        for k in list_k:
            for data_set in data_sets:
                filewriter.writerow([data_set, str(nr_assets), str(k), str(run_time[
                                    data_set, k, 'padm']), str(run_time[data_set, k, 'miqp_solver']), str(
                                    obj_value[data_set, k, 'padm']), str(obj_value[data_set, k, 'miqp_solver']),
                                     str(gap[data_set, k]), str(outer_iters[data_set, k]), str(rho_initial)])
            filewriter.writerow([])
            mean_time_padm = sum(run_time[d, k, 'padm'] for d in data_sets) / nr_random
            mean_obj_value_padm = sum(obj_value[d, k, 'padm'] for d in data_sets) / nr_random
            mean_time_grb = sum(run_time[d, k, 'miqp_solver'] for d in data_sets) / nr_random
            mean_obj_value_grb = sum(obj_value[d, k, 'miqp_solver'] for d in data_sets) / nr_random
            mean_gap_grb = sum(gap[d, k] for d in data_sets) / nr_random
            filewriter.writerow(['average', '.', '.', str(mean_time_padm), str(mean_time_grb), str(mean_obj_value_padm),
                                 str(mean_obj_value_grb), str(mean_gap_grb), '.', '.'])
            filewriter.writerow([])
            filewriter.writerow(['padm_obj/big-M_obj=', str(mean_obj_value_padm/mean_obj_value_grb)])
            filewriter.writerow([])
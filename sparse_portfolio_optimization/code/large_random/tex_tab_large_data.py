#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os
import statistics


def build_table(name_dir, data_sets, nr_assets, list_k, run_time, obj_value, gap, nr_random):

    os.chdir(name_dir + "results/results-random-data/")
    content = r"""\documentclass{article}
    \usepackage{siunitx}
    \usepackage{booktabs}

    \begin{document}"""

    content += r"""
        \begin{table} \centering \begin{tabular}{ccccccccc} \toprule 
        Instance & $n$ & $k$ & \multicolumn{2}{c}{PADM} & & \multicolumn{3}{c}{
        Gurobi} \\ \cmidrule{4-5} \cmidrule{7-9} & & & Sol Time & Obj Value   & & Sol Time & Obj Value & Gap (\%)
        \\ """

    name_random_data = "random-" + str(nr_assets)
    content += r""" \midrule \textsf{""" + name_random_data + '} & $' + str(nr_assets) + '$ & '
    for k in list_k:

        mean_time = sum(run_time[d, k, 'padm'] for d in data_sets) / nr_random
        mean_obj_value = sum(obj_value[d, k, 'padm'] for d in data_sets) / nr_random

        content += str(k) + r""" & \sisetup{round-mode=places,round-precision=2} \num{""" + str(mean_time) + r"""} 
        & \sisetup{round-mode=places,round-precision=2} \num[scientific-notation = fixed, fixed-exponent = 0]{""" + str(
                mean_obj_value) + r"""} & """
        mean_time = sum(run_time[d, k, 'miqp_solver'] for d in data_sets) / nr_random
        mean_obj_value = sum(obj_value[d, k, 'miqp_solver'] for d in data_sets) / nr_random
        mean_gap = sum(gap[d, k] for d in data_sets) / nr_random

        content += r""" & \sisetup{round-mode=places,round-precision=2} \num{""" + str(
            mean_time) + r"""} & \sisetup{round-mode=places,round-precision=2} \num[scientific-notation = fixed, fixed-exponent = 0]{""" + str(
            mean_obj_value) + r"""} & \sisetup{round-mode=places,round-precision=2} \num[scientific-notation = fixed, fixed-exponent = 0]{""" + str(
            mean_gap) + '}'
        if k == list_k[-1]:
            content += r""" \\ """
        else:
            content += r""" \\ & & """

    content += r"""\bottomrule \end{tabular} \end{table}"""
    content += r""" \end{document}"""

    with open('tab-' + str(name_random_data) + '.tex', 'w') as file:
        file.write(content)


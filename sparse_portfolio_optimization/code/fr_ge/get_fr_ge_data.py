#!/usr/bin/env python3

import numpy as np


def get_data(dir_name, name, nr_assets):
    directory_data = dir_name + "/data/Frangioni_Gentile/"
    with open(directory_data + "MV/size" + str(nr_assets) + "/" + name + ".txt", "r") as data_file:
        data_lines = data_file.readlines()
    mean_return_entry = []
    first_line = data_lines[0].split()
    nr_of_assets = int(first_line[0])
    assert(nr_of_assets == nr_assets)
    for data_point in data_lines[1:]:
        entry = data_point.split()
        scalar = float(entry[0])
        np_scalar = np.array(scalar)
        mean_return_entry.append(np_scalar)
    mean_returns_vector = np.array(mean_return_entry)

    rows_sigma_list = []
    with open(directory_data + "MV/size" + str(nr_assets) + "/" + name + ".mat", "r") as data_file_sigma:
        lines_sigma = data_file_sigma.readlines()
    for data_point in lines_sigma[1:]:
        splitted_line = data_point.split()
        vector = []
        for entry in splitted_line:
            vector.append(float(entry))
        np_vector = np.array(vector)
        rows_sigma_list.append(np_vector)

    covariance_matrix = np.array(rows_sigma_list)

    with open(directory_data + "MV/size" + str(nr_assets) + "/" + name + ".rho") as return_value:
        one_line = return_value.readlines()
    split_data = one_line[0].split()
    required_ret = float(split_data[0])

    with open(directory_data + "diagonals/s/" + name + ".diag") as diag_data:
        diag_lines = diag_data.readlines()
    diag_entries = []
    for diag_line in diag_lines[1:]:
        split_line = diag_line.split()
        scalar = float(split_line[0])
        diag_entries.append(scalar)
    diag_vector = np.array(diag_entries)

    return mean_returns_vector, covariance_matrix, required_ret, diag_vector
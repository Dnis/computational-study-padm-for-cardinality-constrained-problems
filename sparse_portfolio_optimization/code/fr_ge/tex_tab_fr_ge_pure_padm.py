#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os


def write_tex_table(dir_name, nr_assets, data_names, data_sets, methods, k, run_time, obj_value):

    os.chdir(dir_name + "/results/results-FrGe-data/pure-PADM/")
    content = r"""\documentclass{article}
    \usepackage{siunitx}
    \usepackage{booktabs}

    \begin{document}"""

    content += r"""
    \begin{tabular}{cccccccccc} \toprule 
     & & \multicolumn{2}{c}{PADM} & & \multicolumn{2}{c}{MISOCP} & & \multicolumn{2}{c}{Big-M}
    \\ \cmidrule{3-4} \cmidrule{6-7} \cmidrule{9-10} Instance & k & Time & Obj. & & Time & Obj. & & 
    Time & Obj. 
    \\ """

    counter_sets = 0
    for data_name in data_names:
        if counter_sets >= 1:
            content += r"""\textsf{""" + data_name + "} & & "
        else:
            content += r"""\midrule \textsf{""" + data_name + "} &" + str(k) + " & "
        for method in methods:
            if method.__name__ == 'padm':
                mean_time = sum(run_time[data_set, k, "padm"] for data_set in
                                data_sets[counter_sets]) / len(data_sets[counter_sets])
                mean_obj = sum(obj_value[data_set, k, "padm"] for data_set in
                               data_sets[counter_sets]) / len(data_sets[counter_sets])
                content += r"""\sisetup{round-mode=places,round-precision=2} \num{"""
                content += str(mean_time) + r"""} & \sisetup{round-mode=places,round-precision=3} \num{"""
                content += str(mean_obj) + '} &'
            elif method.__name__ == "misocp_solver":
                mean_time = sum(run_time[data_set, k, "misocp"] for data_set in
                                data_sets[counter_sets]) / len(data_sets[counter_sets])
                mean_obj = sum(obj_value[data_set, k, "misocp"] for data_set in
                               data_sets[counter_sets]) / len(data_sets[counter_sets])
                content += r""" & \sisetup{round-mode=places,round-precision=2} \num{"""
                content += str(mean_time) + r"""} & \sisetup{round-mode=places, round-precision=3} \num{"""
                content += str(mean_obj) + '} &'
            elif method.__name__ == "bigm_solver":
                mean_time = sum(run_time[data_set, k, "bigm"] for data_set in
                                data_sets[counter_sets]) / len(data_sets[counter_sets])
                mean_obj = sum(obj_value[data_set, k, "bigm"] for data_set in
                               data_sets[counter_sets]) / len(data_sets[counter_sets])
                content += r""" & \sisetup{round-mode=places,round-precision=2} \num{"""
                content += str(mean_time) + r"""} & \sisetup{round-mode=places, round-precision=3} \num{"""
                content += str(mean_obj) + '}'
        content += r""" \\ """
        counter_sets += 1
    content += r"""\bottomrule \end{tabular} \end{document}"""

    with open(str(nr_assets) + 'k_' + str(k) + '.tex', 'w') as file:
        file.write(content)
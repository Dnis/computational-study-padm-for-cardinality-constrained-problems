#!/usr/bin/env python3

import numpy as np
import gurobipy as gp
import time


def misocp_solver(nr_assets, vec_return, required_return, sigma, k, time_limit, diag_vector):
    start = time.time()
    m = gp.Model("misocp")
    m.Params.timeLimit = time_limit

    z_vars = m.addMVar(nr_assets, vtype=gp.GRB.BINARY, name="z_vars")

    x_vars = m.addMVar(nr_assets, lb=0.0, vtype=gp.GRB.CONTINUOUS, name="x_vars")

    aux_theta_vars = m.addMVar(nr_assets, lb=0.0, vtype=gp.GRB.CONTINUOUS, name="aux_theta_vars")

    m.setObjective(x_vars @ (sigma - np.diag(diag_vector)) @ x_vars + aux_theta_vars @ diag_vector, gp.GRB.MINIMIZE)

    m.addConstr(x_vars.sum() == 1)

    m.addConstr(vec_return @ x_vars >= required_return)

    m.addConstr(z_vars.sum() <= k)
    # cone constraints
    m.addConstrs((x_vars[i] @ x_vars[i] <= aux_theta_vars[i] @ z_vars[i] for i in range(nr_assets)))

    m.optimize()

    #x = np.zeros(nr_assets)
    #for i in range(nr_assets):
    #   x[i] = x_vars[i].X

    obj = m.getObjective()
    obj_value = obj.getValue()
    end = time.time()

    return obj_value, end - start

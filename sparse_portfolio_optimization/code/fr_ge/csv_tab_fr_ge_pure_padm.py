#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import csv
import os


def write_csv_table(dir_name, data_names, data_sets, nr_assets, k, run_time, obj_value,
                    penalty_init, outer_iters):
    os.chdir(dir_name + "/results/results-FrGe-data/pure-PADM/")

    with open(str(nr_assets) + '_' + str(k) + ".csv", "w") as csvfile:
        filewriter = csv.writer(csvfile, delimiter='|', quotechar='|', quoting=csv.QUOTE_MINIMAL)
        filewriter.writerow(['instances', 'nr_assets', 'k', 'PADM time(s)',
                             'misocp time', 'bigm time(s)', 'PADM objval', 'misocp objval',
                             'bigm objval', 'out_iters', 'rho_initial'])
        counter_sets = 0
        for data_name in data_names:
            filewriter.writerow([data_name])
            for data_set in data_sets[counter_sets]:
                filewriter.writerow([data_set, str(nr_assets), str(k), str(run_time[data_set, k, 'padm']),
                                     str(run_time[data_set, k, 'misocp']), str(run_time[data_set, k, 'bigm']),
                                     str(obj_value[data_set, k, 'padm']), str(obj_value[data_set, k, 'misocp']),
                                     str(obj_value[data_set, k, 'bigm']),
                                     str(outer_iters[data_set, k]), str(penalty_init)])
            filewriter.writerow([])
            mean_time_padm = sum(run_time[data_set, k, "padm"] for data_set in
                            data_sets[counter_sets]) / len(data_sets[counter_sets])
            mean_obj_padm = sum(obj_value[data_set, k, "padm"] for data_set in
                           data_sets[counter_sets]) / len(data_sets[counter_sets])
            mean_time_miscop = sum(run_time[data_set, k, "misocp"] for data_set in
                            data_sets[counter_sets]) / len(data_sets[counter_sets])
            mean_obj_misocp = sum(obj_value[data_set, k, "misocp"] for data_set in
                           data_sets[counter_sets]) / len(data_sets[counter_sets])
            mean_time_bigm = sum(run_time[data_set, k, "bigm"] for data_set in
                            data_sets[counter_sets]) / len(data_sets[counter_sets])
            mean_obj_bigm = sum(obj_value[data_set, k, "bigm"] for data_set in
                           data_sets[counter_sets]) / len(data_sets[counter_sets])
            filewriter.writerow(['average', '.', '.', str(mean_time_padm), str(mean_time_miscop),
                                 str(mean_time_bigm), str(mean_obj_padm), str(mean_obj_misocp),
                                 str(mean_obj_bigm), '.', '.'])
            filewriter.writerow([])
            counter_sets += 1
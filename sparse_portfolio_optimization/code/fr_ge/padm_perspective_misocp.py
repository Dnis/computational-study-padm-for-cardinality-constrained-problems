#!/usr/bin/env python3

import gurobipy as gp
from numpy import linalg as LA
import numpy as np
import time


def create_x_subproblem(nr_assets, mu, r_min):
    # Create an empty model
    m = gp.Model("subproblem_direction_x")

    x_vars = m.addMVar(nr_assets, lb=0.0, vtype=gp.GRB.CONTINUOUS, name="x_vars")

    y_plus_vars = m.addMVar(nr_assets, lb=0.0, vtype=gp.GRB.CONTINUOUS, name="y_plus_vars")

    y_minus_vars = m.addMVar(nr_assets, lb=0.0, vtype=gp.GRB.CONTINUOUS, name="y_minus_vars")

    # Set constraints
    m.addConstr(x_vars.sum() == 1)
    m.addConstr(mu @ x_vars >= r_min)
    m.update()
    return m, x_vars, y_plus_vars, y_minus_vars


def create_misocp_w_subproblem(nr_assets, k):
    # Create an empty model
    misocp_w = gp.Model("misocp_direction_w")

    w_vars = misocp_w.addMVar(nr_assets, lb=0.0, vtype=gp.GRB.CONTINUOUS, name="w_vars")

    z_vars = misocp_w.addMVar(nr_assets, vtype=gp.GRB.BINARY, name="z_vars")

    theta_vars = misocp_w.addMVar(nr_assets, lb=0.0, vtype=gp.GRB.CONTINUOUS, name="theta_vars")

    a_plus_vars = misocp_w.addMVar(nr_assets, lb=0.0, vtype=gp.GRB.CONTINUOUS, name="a_plus_vars")

    a_minus_vars = misocp_w.addMVar(nr_assets, lb=0.0, vtype=gp.GRB.CONTINUOUS, name="a_minus_vars")

    # Set constraints
    misocp_w.addConstr(w_vars.sum() == 1)
    misocp_w.addConstr(z_vars.sum() <= k)
    # cone constraints
    misocp_w.addConstrs((w_vars[i] @ w_vars[i] <= theta_vars[i] @ z_vars[i] for i in range(nr_assets)))
    misocp_w.update()
    return misocp_w, w_vars, z_vars, a_plus_vars, a_minus_vars, theta_vars


def solve_subproblem_x(m, x_vars, y_plus_vars, y_minus_vars, nr_assets, matrix_sigma,
                       penalty_param, w, diag_vector):
    c = m.addConstr(x_vars - w == y_plus_vars - y_minus_vars)
    # Set objective
    vec_penalty_param = np.full(nr_assets, penalty_param)
    m.setObjective(x_vars @ (matrix_sigma - np.diag(diag_vector)) @ x_vars + vec_penalty_param @ y_plus_vars
                   + vec_penalty_param @ y_minus_vars, gp.GRB.MINIMIZE)
    ##m.write("out.lp")
    m.optimize()

    # Extract solution
    x = np.zeros(nr_assets)
    for i in range(nr_assets):
        x[i] = x_vars[i].X

    m.remove(c)
    m.update()

    return x


def solve_discrete_subproblem_w(misocp_w, nr_assets, w_vars, x, a_plus_vars, a_minus_vars, theta_vars,
                                diag_vector, penalty_param):
    c1 = misocp_w.addConstr(x - w_vars == a_plus_vars - a_minus_vars)
    vec_penalty_param = np.full(nr_assets, penalty_param)
    misocp_w.setObjective(theta_vars @ diag_vector + vec_penalty_param @ a_plus_vars
                          + vec_penalty_param @ a_minus_vars, gp.GRB.MINIMIZE)
    #misocp_w.write("out.lp")
    misocp_w.optimize()

    # Extract solution
    w = np.zeros(nr_assets)
    for i in range(nr_assets):
        w[i] = w_vars[i].X

    misocp_w.remove(c1)
    misocp_w.update()

    return w


def partial_minimum(x_new, x, w_new, w, tol):
    dif1 = x_new - x
    dif2 = w_new - w
    dif = np.concatenate((dif1, dif2))
    return LA.norm(dif, np.inf) < tol


def coupling_satisfied(x_new, w_new, tol):
    dif = x_new - w_new
    return LA.norm(dif, 1) <= tol


def padm_perspective(nr_assets, vec_returns, expected_ret, matrix_sigma, diag_vector, k, penalty_param, time_limit):
    start = time.time()

    m, x_vars, y_plus_vars, y_minus_vars = create_x_subproblem(nr_assets, vec_returns, expected_ret)
    misocp_w, w_vars, z_vars, a_plus_vars, a_minus_vars, theta_vars = create_misocp_w_subproblem(nr_assets, k)

    # Initialize w to zeros
    w_zeros = np.zeros(nr_assets)
    w = w_zeros

    x = solve_subproblem_x(m, x_vars, y_plus_vars, y_minus_vars, nr_assets, matrix_sigma, penalty_param, w, diag_vector)

    # Initialize outer iteration count
    outer_iter_counter = 1
    adm_iterations = []

    while penalty_param < 1e10 and outer_iter_counter < 1e8:
        # Initialize inner iteration counter
        inner_iter_counter = 1
        while inner_iter_counter < 1e8:

            w_new = solve_discrete_subproblem_w(misocp_w, nr_assets, w_vars, x, a_plus_vars, a_minus_vars,
                                                theta_vars, diag_vector, penalty_param)

            x_new = solve_subproblem_x(m, x_vars, y_plus_vars, y_minus_vars, nr_assets,
                                       matrix_sigma, penalty_param, w_new, diag_vector)
            # Check for partial minimum
            if partial_minimum(x_new, x, w_new, w, tol=1e-5) or time.time() - start >= time_limit:
                break
            w = w_new
            x = x_new
            inner_iter_counter += 1

        adm_iterations.append(inner_iter_counter)

        if coupling_satisfied(x_new, w_new, tol=1e-5) or time.time() - start >= time_limit:
            break
        penalty_param *= 10
        outer_iter_counter += 1

    obj_value = x_new @ matrix_sigma @ x_new

    end = time.time()
    return adm_iterations, outer_iter_counter, end - start, obj_value

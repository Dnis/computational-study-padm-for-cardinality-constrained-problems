#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import csv
import os


def write_csv_table(dir_name, data_names, data_sets, nr_assets, k, methods, run_time, obj_value,
                    penalty_init, outer_iters):
    os.chdir(dir_name + "/results/results-FrGe-data/PADM-PERSP/")

    with open('persp-padm-' + str(nr_assets) + '_' + str(k) + ".csv", "w") as csvfile:
        filewriter = csv.writer(csvfile, delimiter='|', quotechar='|', quoting=csv.QUOTE_MINIMAL)
        filewriter.writerow(['instances', 'nr_assets', 'k', 'padm_persp time(s)',
                             'padm_persp obj val', 'out_iters_padm_persp', 'rho_initial'])
        counter_sets = 0
        for data_name in data_names:
            filewriter.writerow([data_name])
            for data_set in data_sets[counter_sets]:
                for method in methods:
                    if method.__name__ == "padm_perspective":
                        filewriter.writerow(
                            [data_set, str(nr_assets), str(k), str(run_time[data_set, k, 'padm_perspective']),
                             str(obj_value[data_set, k, 'padm_perspective']),
                             str(outer_iters[data_set, k, 'padm_perspective']),
                             str(penalty_init)])
            filewriter.writerow([])
            mean_time = sum(run_time[data_set, k, "padm_perspective"] for data_set in
                            data_sets[counter_sets]) / len(data_sets[counter_sets])
            mean_obj = sum(obj_value[data_set, k, "padm_perspective"] for data_set in
                           data_sets[counter_sets]) / len(data_sets[counter_sets])
            filewriter.writerow(['average', '.', '.', str(mean_time), str(mean_obj), '.', '.'])
            filewriter.writerow([])
            counter_sets += 1


#  Copyright 2021 Carina Moreira Costa, Dennis Kreber, Martin Schmidt
#
#  Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
#  The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
#
#  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

from lab import lab
import pandas as pd

if __name__ == '__main__':
    table = lab.mean_table("frieda"). \
        set_methods_key("method", label="Method"). \
        set_value_of_interest("computations_runtime"). \
        add_unit_to_compare("PADM", method="padm", start_penalization_correction=0.3). \
        add_unit_to_compare("MIP", method="mip", warmstart="padm"). \
        add_unit_to_compare("MaxMin", method="max_min"). \
        add_unit_to_compare("L0Learn", method="l0learn"). \
        add_unit_to_compare("L0Learn finetuned", method="l0learn_finetuned"). \
        set_instance_keys(("p", "n")). \
        build_table()
    with pd.option_context('display.max_rows', None, 'display.max_columns', None, 'display.expand_frame_repr', False):
        print(table)

#  Copyright 2021 Carina Moreira Costa, Dennis Kreber, Martin Schmidt
#
#  Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
#  The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
#
#  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

import math
import os

from lab import lab


def gap(rss, **kwargs):
    if math.isnan(rss):
        return 1e7
    return (rss - min(kwargs.values())) / rss * 100.0


if __name__ == '__main__':
    if not os.path.exists("gap/"):
        os.mkdir("gap")
    lab.procedural_box_plot("frieda"). \
        set_xaxis("snr", "SNR"). \
        set_yaxis("gap", "Gap (%)"). \
        set_procedure_on_methods(gap, "gap"). \
        set_attribute_to_give_to_procedure("rss"). \
        set_index_columns(["id", "n", "p", "k", "snr"]). \
        set_methods_key("method", "Method"). \
        add_unit_to_compare("PADM", method="padm", start_penalization_correction=0.3). \
        add_unit_to_compare("L0Learn", method="l0learn"). \
        add_unit_to_compare("L0Learn finetuned", method="l0learn_finetuned"). \
        add_unit_to_compare("MIP", method="mip", warmstart="padm"). \
        add_unit_to_compare("MaxMin", method="max_min"). \
        set_hue_order(["padm", "l0learn", "l0learn_finetuned", "mip", "max_min"]). \
        set_y_range((-0.1, 8)). \
        set_font_scale(1.3). \
        hide_ylabel(True). \
        hide_legend(True). \
        set_save_fig_size("a3"). \
        filter_data(k=[30, 50, 250], n=[180, 300, 1500]). \
        add_grid("p", col_label="p"). \
        save("gap/gap_small_all.pdf")

    lab.procedural_box_plot("frieda"). \
        set_xaxis("snr", "SNR"). \
        set_yaxis("gap", "Gap (%)"). \
        set_procedure_on_methods(gap, "gap"). \
        set_attribute_to_give_to_procedure("rss"). \
        set_index_columns(["id", "n", "p", "k", "snr"]). \
        set_methods_key("method", "Method"). \
        add_unit_to_compare("PADM", method="padm", start_penalization_correction=0.3). \
        add_unit_to_compare("L0Learn", method="l0learn"). \
        add_unit_to_compare("L0Learn finetuned", method="l0learn_finetuned"). \
        set_hue_order(["padm", "l0learn", "l0learn_finetuned"]). \
        set_font_scale(1.3). \
        hide_ylabel(True). \
        hide_legend(True). \
        set_save_fig_size("a3"). \
        filter_data(p=[2000, 8000, 15000]). \
        add_grid("p", col_label="p"). \
        save("gap/gap_big_all.pdf")

    lab.procedural_box_plot("frieda"). \
        set_xaxis("snr", "SNR"). \
        set_yaxis("gap", "Gap (%)"). \
        set_procedure_on_methods(gap, "gap"). \
        set_attribute_to_give_to_procedure("rss"). \
        set_index_columns(["id", "n", "p", "k", "snr"]). \
        set_methods_key("method", "Method"). \
        add_unit_to_compare("PADM", method="padm", start_penalization_correction=0.3). \
        add_unit_to_compare("L0Learn", method="l0learn"). \
        add_unit_to_compare("L0Learn finetuned", method="l0learn_finetuned"). \
        set_hue_order(["padm", "l0learn", "l0learn_finetuned"]). \
        set_font_scale(1.3). \
        hide_ylabel(True). \
        hide_legend(True). \
        set_save_fig_size("a3"). \
        filter_data(p=[1000, 10000]). \
        add_grid("p", col_label="p"). \
        save("gap/gap_deg.pdf")

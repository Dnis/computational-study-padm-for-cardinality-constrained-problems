#  Copyright 2021 Carina Moreira Costa, Dennis Kreber, Martin Schmidt
#
#  Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
#  The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
#
#  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
import os

from lab import lab


def relative_sparsity(subset_size, number_of_nonzeros):
    return subset_size / number_of_nonzeros


if __name__ == '__main__':
    if not os.path.exists("relative_sparsity/"):
        os.mkdir("relative_sparsity")
    lab.box_plot("frieda"). \
        set_xaxis("snr", "SNR"). \
        add_procedural_column("relative_sparsity", relative_sparsity, "subset_size", "number_of_nonzeros"). \
        set_yaxis("relative_sparsity", "Relative support size"). \
        set_methods_key("method", "Method"). \
        add_unit_to_compare("PADM", method="padm", start_penalization_correction=0.3). \
        add_unit_to_compare("L0Learn", method="l0learn"). \
        add_unit_to_compare("L0Learn finetuned", method="l0learn_finetuned"). \
        set_hue_order(["padm", "l0learn", "l0learn_finetuned"]). \
        set_font_scale(1.3). \
        hide_ylabel(True). \
        hide_legend(True). \
        add_grid("p"). \
        filter_data(p=[500, 2000, 8000, 15000]). \
        save("relative_sparsity/relative_sparsity_all_big.pdf")

    lab.box_plot("frieda"). \
        set_xaxis("snr", "SNR"). \
        add_procedural_column("relative_sparsity", relative_sparsity, "subset_size", "number_of_nonzeros"). \
        set_yaxis("relative_sparsity", "Relative support size"). \
        set_methods_key("method", "Method"). \
        add_unit_to_compare("PADM", method="padm", start_penalization_correction=0.3). \
        add_unit_to_compare("L0Learn", method="l0learn"). \
        add_unit_to_compare("L0Learn finetuned", method="l0learn_finetuned"). \
        set_hue_order(["padm", "l0learn", "l0learn_finetuned"]). \
        set_font_scale(1.3). \
        hide_ylabel(True). \
        hide_legend(True). \
        add_grid("p"). \
        filter_data(p=[1000, 10000]). \
        save("relative_sparsity/relative_sparsity_all_deg.pdf")

    lab.box_plot("frieda"). \
        set_xaxis("snr", "SNR"). \
        add_procedural_column("relative_sparsity", relative_sparsity, "subset_size", "number_of_nonzeros"). \
        set_yaxis("relative_sparsity", "Relative support size"). \
        set_methods_key("method", "Method"). \
        add_unit_to_compare("PADM", method="padm", start_penalization_correction=0.3). \
        add_unit_to_compare("L0Learn", method="l0learn"). \
        add_unit_to_compare("L0Learn finetuned", method="l0learn_finetuned"). \
        set_hue_order(["padm", "l0learn", "l0learn_finetuned"]). \
        set_font_scale(1.3). \
        hide_ylabel(True). \
        hide_legend(True). \
        add_grid("p"). \
        filter_data(k=[30, 50, 250], n=[180, 300, 1500]). \
        save("relative_sparsity/relative_sparsity_small_all.pdf")

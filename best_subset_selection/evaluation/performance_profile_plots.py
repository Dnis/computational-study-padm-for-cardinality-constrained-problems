#  Copyright 2021 Carina Moreira Costa, Dennis Kreber, Martin Schmidt
#
#  Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
#  The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
#
#  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
import math
import os

from lab import lab


def performance_ratio(runtime, **kwargs):
    if math.isnan(runtime):
        return 1e7
    if runtime - 3600 >= -2:
        return 1e7
    return runtime / min(kwargs.values())


if __name__ == '__main__':
    if not os.path.exists("performance_profile/"):
        os.mkdir("performance_profile")
    lab.procedural_ecdf_plot("frieda"). \
        set_procedure_on_methods(performance_ratio, "performance_ratio"). \
        set_attribute_to_give_to_procedure("computations_runtime"). \
        set_index_columns(["id", "n", "p", "k", "snr"]). \
        set_methods_key("method", "Method"). \
        set_value_of_interest("performance_ratio", "Performance ratio"). \
        add_unit_to_compare("PADM", method="padm", start_penalization_correction=0.3). \
        add_unit_to_compare("L0Learn", method="l0learn"). \
        add_unit_to_compare("L0Learn finetuned", method="l0learn_finetuned"). \
        add_unit_to_compare("MIP", method="mip", warmstart="padm"). \
        add_unit_to_compare("MaxMin", method="max_min"). \
        set_hue_order(["padm", "l0learn", "l0learn_finetuned", "mip", "max_min"]). \
        set_line_width(3). \
        hide_ylabel(True). \
        hide_legend(False). \
        set_font_scale(1.3). \
        set_save_fig_size("a3"). \
        set_x_range((0.95, 1000000)). \
        set_x_scale("log"). \
        filter_data(k=[30, 50, 250, 600, 2500, 5000], n=[180, 300, 1500, 6000, 24000, 45000]). \
        save("performance_profile/performance_profile.pdf")

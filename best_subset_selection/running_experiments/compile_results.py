from dlab.helper.conversion import numpy_dict_to_json
from pianopy.metrics import *


def compile_results_from_solution(sol, regression_data):
    beta = sol["full_coefficients"]
    beta.shape = sol["p"]
    beta0 = regression_data["beta"]
    beta0.shape = beta.shape

    results = dict()

    results["relative_risk"] = relative_risk(sol, regression_data)
    results["relative_test_error"] = relative_test_error(sol, regression_data)
    results["proportion_of_variance_explained"] = proportion_of_variance_explained(sol, regression_data)

    results["selection_accuracy"] = selection_accuracy(sol, regression_data)
    results["selection_errors"] = selection_errors(sol, regression_data)
    sol.pop("Gamma", None)
    results["solution"] = sol
    results["beta0"] = beta0
    results["correct_subset"] = regression_data["S"]
    results["nullscore_minus_rss"] = sol["null_score"] - sol["rss"]
    return numpy_dict_to_json(results)

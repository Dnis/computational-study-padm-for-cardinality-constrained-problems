#  Copyright 2021 Carina Moreira Costa, Dennis Kreber, Martin Schmidt
#
#  Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
#  The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
#
#  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
import json

import pkg_resources
pkg_resources.require("digitallab==1.4.3.0")
pkg_resources.require("pianopy==0.8.4.6.2")

from packaging import version
import digitallab
from pianopy.data_generation import generate_regression_data_from_dict

from compile_results import compile_results_from_solution
from execution import execute

assert version.parse(digitallab.__version__) >= version.parse("1.4.1.0"), \
    "You are using an incompatible version of digitallab. " \
    "Please install digitallab 1.4.1.0 by calling 'pip install --user digitallab==1.4.3.0'."

from digitallab.lab import Lab

with open("../mongodb_credentials.json", "r") as file:
    mongodb_credentials = json.load(file)

mongodb_url = 'mongodb://'
mongodb_url += mongodb_credentials["user_name"]
if mongodb_credentials["password"]:
    mongodb_url += ":" + mongodb_credentials["password"]
mongodb_url += "@" + mongodb_credentials["server"]
mongodb_url += "/" + mongodb_credentials["database"]
if mongodb_credentials["auth_mechanism"]:
    mongodb_url += "?authMechanism=" + mongodb_credentials["auth_mechanism"]

lab = Lab("padm_computational_study").add_mongodb_storage(mongodb_url, mongodb_credentials["database"])


@lab.experiment
def main(_config):
    regression_data = generate_regression_data_from_dict(**_config)
    solution = execute(regression_data, **_config)
    return compile_results_from_solution(solution, regression_data)


lab.run_experiments(main, "experiments_with_low_memory_requirements", 22, show_selection_dialog=True)
lab.run_experiments(main, "experiments_with_large_memory_requirements", number_of_parallel_runs=2,
                    number_of_query_chunks=5, number_of_query_workers=5, show_selection_dialog=True)

from pianopy.bss.inexact import padm
from pianopy.bss.inexact import max_min
from pianopy.bss.exact import mip

from l0learn import *


def exec_padm(regression_data, k=1, ridge_parameter=0.0, timelimit=120, intercept=False, normalise=True, mute=True,
              start_penalization_correction=0.8, number_of_threads=1, lasso_solver="picasso",
              **kwargs):
    return padm(regression_data["X"], regression_data["y"], k, ridge_parameter=ridge_parameter, timelimit=timelimit,
                intercept=intercept, normalise=normalise, mute=mute,
                start_penalization_correction=start_penalization_correction, number_of_threads=number_of_threads,
                lasso_solver=lasso_solver)


def exec_bss_mip(regression_data, k=1, ridge_parameter=0.0, timelimit=120, intercept=False, normalise=True,
                 number_of_threads=1, mute=True, mipstart="none", **kwargs):
    if mipstart == "none":
        return mip(regression_data["X"], regression_data["y"], k, ridge_parameter=ridge_parameter, timelimit=timelimit,
                   intercept=intercept, normalise=normalise, number_of_threads=number_of_threads, mute=mute,
                   indicator_constraints_handling="BigM")
    else:
        sol = padm(regression_data["X"], regression_data["y"], k, ridge_parameter=ridge_parameter, timelimit=timelimit,
                   intercept=intercept, normalise=normalise, number_of_threads=number_of_threads, mute=mute)
        return mip(regression_data["X"], regression_data["y"], k, ridge_parameter=ridge_parameter, timelimit=timelimit,
                   intercept=intercept, normalise=normalise, number_of_threads=number_of_threads, mute=mute,
                   indicator_constraints_handling="BigM", warmstart=sol)


def exec_max_min(regression_data, k=1, ridge_parameter=0.0, timelimit=120, intercept=False, normalise=True, mute=True,
                 number_of_threads=1, extract_eigenvalue=True, **kwargs):
    return max_min(regression_data["X"], regression_data["y"], k, ridge_parameter=ridge_parameter, timelimit=timelimit,
                   intercept=intercept, normalise=normalise, mute=mute, number_of_threads=number_of_threads,
                   extract_eigenvalue=extract_eigenvalue)


def exec_l0learn(regression_data, k=1, intercept=False, do_validation=False, **kwargs):
    return l0learn(regression_data["X"], regression_data["y"], k, intercept=intercept, do_validation=do_validation)


def exec_l0learn_finetuned(regression_data, k=1, intercept=False, do_validation=False, **kwargs):
    return l0learn_finetuned(regression_data["X"], regression_data["y"], k, intercept=intercept,
                             do_validation=do_validation)


def execute(regression_data, **kwargs):
    if "method" not in kwargs:
        raise KeyError
    if kwargs["method"] == "padm":
        return exec_padm(regression_data, **kwargs)
    if kwargs["method"] == "mip":
        return exec_bss_mip(regression_data, **kwargs)
    if kwargs["method"] == "max_min":
        return exec_max_min(regression_data, **kwargs)
    if kwargs["method"] == "l0learn":
        return exec_l0learn(regression_data, do_validation=False, **kwargs)
    if kwargs["method"] == "l0learn_cv":
        return exec_l0learn(regression_data, do_validation=True, **kwargs)
    if kwargs["method"] == "l0learn_finetuned":
        return exec_l0learn_finetuned(regression_data, do_validation=False, **kwargs)
    raise KeyError

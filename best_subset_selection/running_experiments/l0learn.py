import timeit

import numpy as np
import rpy2.robjects as ro
import rpy2.robjects.numpy2ri

rpy2.robjects.numpy2ri.activate()


class Timer:
    def __init__(self):
        self.elapsed_time = 0
        self.__start = None
        self.__stop = None

        self.__running = False

    def start(self):
        assert not self.__running, "Timer is running. Stop it first before running it again."
        self.__running = True
        self.__start = timeit.default_timer()

    def stop(self):
        assert self.__running, "Timer is not running. Start it first before ending it."
        self.__end = timeit.default_timer()
        self.elapsed_time += self.__end - self.__start
        self.__running = False

    def reset(self):
        self.elapsed_time = 0


def l0learn(X, y, k, intercept=False, do_validation=False):
    complete_timer = Timer()
    only_computation_timer = Timer()

    complete_timer.start()

    # package = importr('L0Learn')
    ro.r("library(L0Learn)")

    n, p = X.shape
    rX = ro.r.matrix(X, nrow=n, ncol=p)
    ro.r.assign("X", rX)

    ry = ro.r.c(y)
    ro.r.assign("y", ry)

    ro.r.assign("k", k)

    only_computation_timer.start()
    if do_validation:
        if intercept:
            ro.r('''
                fit <- L0Learn.cvfit(X, y, nFolds=5, penalty="L0")
                ''')
        else:
            ro.r('''
            fit <- L0Learn.cvfit(X, y, nFolds=5, penalty="L0", intercept=FALSE)
            ''')
        beta = np.array(ro.r('''
        optimalLambdaIndex = which.min(fit$cvMeans[[1]])
        optimalLambda = fit$fit$lambda[[1]][optimalLambdaIndex]
        as.vector(coef(fit, lambda=optimalLambda))
        '''))
    else:
        if intercept:
            ro.r('''
                fit <- L0Learn.fit(X, y, maxSuppSize=k, penalty="L0")
                ''')
        else:
            ro.r('''
            fit <- L0Learn.fit(X, y, maxSuppSize=k, penalty="L0", intercept=FALSE)
            ''')
        beta = np.array(ro.r('''
        lambdas <- fit$lambda[[1]]
        as.vector(coef(fit, lambda=lambdas[length(lambdas)]))
        '''))
    only_computation_timer.stop()
    complete_timer.stop()

    sol = generate_solution(X, y, beta, complete_timer.elapsed_time, only_computation_timer.elapsed_time)

    return sol


def l0learn_finetuned(X, y, k, intercept=False, do_validation=False):
    complete_timer = Timer()
    only_computation_timer = Timer()

    complete_timer.start()

    # package = importr('L0Learn')
    ro.r("library(L0Learn)")

    n, p = X.shape
    rX = ro.r.matrix(X, nrow=n, ncol=p)
    ro.r.assign("X", rX)

    ry = ro.r.c(y)
    ro.r.assign("y", ry)

    ro.r.assign("k", k)

    only_computation_timer.start()
    if do_validation:
        if intercept:
            ro.r('''
                fit <- L0Learn.cvfit(X, y, nFolds=5, penalty="L0")
                ''')
        else:
            ro.r('''
            fit <- L0Learn.cvfit(X, y, nFolds=5, penalty="L0", intercept=FALSE)
            ''')
        beta = np.array(ro.r('''
        optimalLambdaIndex = which.min(fit$cvMeans[[1]])
        optimalLambda = fit$fit$lambda[[1]][optimalLambdaIndex]
        as.vector(coef(fit, lambda=optimalLambda))
        '''))
    else:
        if intercept:
            ro.r('''
                fit <- L0Learn.fit(X, y, maxSuppSize=2*k, penalty="L0", scaleDownFactor=0.95, nLambda=floor(k / 2))
                ''')
        else:
            ro.r('''
            fit <- L0Learn.fit(X, y, maxSuppSize=2*k, penalty="L0", intercept=FALSE, scaleDownFactor=0.95, nLambda=floor(k / 2))
            ''')
        ro.r('''
        index <- 0
        for (i in 1:length(fit$suppSize[[1]])) {
            if (fit$suppSize[[1]][i] > k) {
                break
            }
            index <- index + 1
        }
        ''')
        beta = np.array(ro.r('''
        lambdas <- fit$lambda[[1]]
        as.vector(coef(fit, lambda=lambdas[index]))
        '''))
    only_computation_timer.stop()
    complete_timer.stop()

    sol = generate_solution(X, y, beta, complete_timer.elapsed_time, only_computation_timer.elapsed_time)

    return sol


def generate_solution(X, y, beta, required_time_complete, required_time_computations):
    sol = dict()
    nonzeros = np.nonzero(beta)[0]
    indicator_vector = np.zeros(len(beta))
    indicator_vector[nonzeros] = 1

    sol["objective_value"] = -1
    sol["rss"] = np.linalg.norm(X.dot(beta) - y, 2) ** 2 / y.dot(y)
    sol["subset"] = nonzeros
    sol["coefficients"] = beta[nonzeros]
    sol["n"] = len(y)
    sol["p"] = len(beta)
    sol["subset_size"] = len(nonzeros)
    sol["estimated_prediction_error"] = -1
    sol["gamma"] = np.zeros(len(beta))
    sol["provable_optimal"] = False
    sol["intercept"] = 0
    sol["complete_runtime"] = required_time_complete
    sol["computations_runtime"] = required_time_computations
    sol["mse"] = sol["rss"] / sol["n"]
    sol["full_coefficients"] = beta
    sol["ridge_parameter"] = 0
    sol["indicator_vector"] = indicator_vector
    sol["gap"] = -1
    sol["sanity_check"] = True
    sol["null_score"] = np.linalg.norm(y, 2) ** 2
    sol["timelimit_reached"] = False
    sol["max_iteration_count_reached"] = False

    return sol
